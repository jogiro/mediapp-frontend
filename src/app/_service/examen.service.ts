import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { Examen } from '../_model/examen';
import { HttpClient } from '@angular/common/http';
import { ConsultaListaExamenDTO } from '../_model/consultaListaExamenDTO';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {

  examenesCambio = new Subject<Examen[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST_URL}/examenes`;

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<Examen[]>(this.url);
  }

  listarId(idExamen: number){
    return this.http.get<Examen>(`${this.url}/${idExamen}`);
  }

  registrar(examen: Examen){
    return this.http.post(`${this.url}`, examen);
  }

  modificar(examen: Examen){
    return this.http.put(`${this.url}`, examen);
  }

  eliminar(idExamen: number){
    return this.http.delete(`${this.url}/${idExamen}`);
  }

  listarExamenesPorConsulta(idConsulta: number){
    return this.http.get<ConsultaListaExamenDTO>(`${environment.HOST_URL}/consultaexamenes/${idConsulta}`);
  }

}
