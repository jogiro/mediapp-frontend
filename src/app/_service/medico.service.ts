import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Medico } from '../_model/medico';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  medicosCambio = new Subject<Medico[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST_URL}/medicos`

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<Medico[]>(this.url);
  }

  listarId(IdMedico: number){
    return this.http.get<Medico>(`${this.url}/${IdMedico}`);
  }

  registrar(medico: Medico){
    return this.http.post(`${this.url}`, medico);
  }

  modificar(medico: Medico){
    return this.http.put(`${this.url}`, medico);
  }

  eliminar(idMedico: number){
    return this.http.delete(`${this.url}/${idMedico}`);
  }
}
