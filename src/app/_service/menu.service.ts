import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { Menu } from '../_model/menu';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menuCambio = new Subject<Menu[]>();

  url: string = `${environment.HOST_URL}`

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<Menu[]>(`${this.url}/menus`)
  }

  listarPorUsuario(nombre: string){
    return this.http.post<Menu[]>(`${this.url}/usuario`, nombre);
  }

}
