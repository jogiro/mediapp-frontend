import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Especialidad } from '../_model/especialidad';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {

  EspecialidadesCambio = new Subject<Especialidad[]>();
  MensajeCambio = new Subject<string>();
  
  url: string = `${environment.HOST_URL}/especialidades`

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<Especialidad[]>(this.url);
  }

  listarId(idEspecialidad: number){
    return this.http.get<Especialidad>(`${this.url}/${idEspecialidad}`);
  }

  registrar(especialidad: Especialidad){
    return this.http.post(`${this.url}`, especialidad);
  }

  modificar(especialidad: Especialidad){
    return this.http.put(`${this.url}`, especialidad);
  }

  eliminar(idEspecialidad: number){
    return this.http.delete(`${this.url}/${idEspecialidad}`);
  }

}
