import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './login.service';
import { MenuService } from './menu.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Menu } from '../_model/menu';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate{

  constructor(private router: Router, private loginService: LoginService, private menuService: MenuService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    
    const helper= new JwtHelperService();
    let rpta = this.loginService.estaLogeado();

    if(!rpta){
      sessionStorage.clear();
      this.router.navigate(['login']);
      return false;
    }else{
      let token = JSON.parse(sessionStorage.getItem(environment.TOKEN_NAME));

      //Si no esta expirado
      if(!helper.isTokenExpired(token.access_token)){

        const decodedToken = helper.decodeToken(token.access_token);

        let url = state.url;

        return this.menuService.listarPorUsuario(decodedToken.user_name).pipe(map((data: Menu[]) => {
          this.menuService.menuCambio.next(data);

          let cont = 0;

          for(let m of data){
            if(m.url === url){
              cont ++;
              break;
            }
          }

          if(cont > 0){
            return true;
          }else{
            this.router.navigate(['not-401']);
            return false;
          }

        }));

      }else{
        //Si el token ya expiró
        sessionStorage.clear();
        this.router.navigate(['login']);
        return false;

      }

    }

  }

}
