import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { EspecialComponent } from './pages/consulta/especial/especial.component';
import { BuscarComponent } from './pages/buscar/buscar.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { Not401Component } from './pages/not401/not401.component';
import { LoginComponent } from './login/login.component';
import { GuardService } from './_service/guard.service';


const routes: Routes = [
  {path: 'paciente', component: PacienteComponent, children: [
    {path: 'edicion/:id', component: PacienteEdicionComponent},
    {path: 'nuevo', component: PacienteEdicionComponent}
  ], canActivate: [GuardService]},
  {path: 'medico', component: MedicoComponent},
  {path: 'examen', component: ExamenComponent, children: [
    {path: 'edicion/:id', component: ExamenEdicionComponent},
    {path: 'nuevo', component: ExamenEdicionComponent}
  ], canActivate: [GuardService]},
  {path: 'especialidad', component: EspecialidadComponent, children: [
    {path: 'edicion/:id', component: EspecialidadEdicionComponent},
    {path: 'nuevo', component: EspecialidadEdicionComponent}
  ], canActivate: [GuardService]},
  {path: 'consulta', component: ConsultaComponent, canActivate: [GuardService]},
  {path: 'consulta-especial', component: EspecialComponent, canActivate: [GuardService]},
  {path: 'buscar', component: BuscarComponent, canActivate: [GuardService]},
  {path: 'reporte', component: ReporteComponent, canActivate: [GuardService]},
  {path: 'not-401', component: Not401Component},
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
