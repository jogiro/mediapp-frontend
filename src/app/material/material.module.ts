import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatSnackBarModule, MatFormFieldModule, MatIconModule, MatToolbarModule, MatPaginatorIntl, MAT_DATE_LOCALE, MatInputModule, MatButtonModule, MatCardModule, MatDialogModule, MatSelectModule, MatDatepickerModule, MatExpansionModule, MatNativeDateModule, MatAutocompleteModule, MatMenuModule, MatSidenavModule, MatDividerModule } from '@angular/material';
import { MatPaginatorImpl } from '../_shared/mat-paginator';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatMenuModule,
    MatSidenavModule,
    MatDividerModule
  ],
  exports:[
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatMenuModule,
    MatSidenavModule,
    MatDividerModule
  ],
  providers: [
    {provide: MatPaginatorIntl, useClass: MatPaginatorImpl},
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'}
  ]
})
export class MaterialModule { }
