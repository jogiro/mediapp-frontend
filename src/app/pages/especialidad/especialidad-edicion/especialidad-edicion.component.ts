import { Component, OnInit } from '@angular/core';
import { Especialidad } from 'src/app/_model/especialidad';
import { FormGroup, FormControl } from '@angular/forms';
import { EspecialidadService } from 'src/app/_service/especialidad.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-especialidad-edicion',
  templateUrl: './especialidad-edicion.component.html',
  styleUrls: ['./especialidad-edicion.component.css']
})
export class EspecialidadEdicionComponent implements OnInit {

  id: number;
  especialidad: Especialidad;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private especialidadService: EspecialidadService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.especialidad = new Especialidad();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl('')
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  initForm(){
    if(this.edicion){
      this.especialidadService.listarId(this.id).subscribe( data => {
        let id = data.idEspecialidad;
        let nombre = data.nombre;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre)
        });
      });
    }
  }

  operar(){
    this.especialidad.idEspecialidad = this.form.value['id'];
    this.especialidad.nombre = this.form.value['nombre'];

    if(this.especialidad != null && this.especialidad.idEspecialidad > 0){
      this.especialidadService.modificar(this.especialidad).subscribe(() => {
        this.especialidadService.listar().subscribe( data => {
          this.especialidadService.EspecialidadesCambio.next(data);
          this.especialidadService.MensajeCambio.next("Se modificó");
        });
      });
    }else{
      this.especialidadService.registrar(this.especialidad).subscribe( ()=> {
        this.especialidadService.listar().subscribe(data => {
          this.especialidadService.EspecialidadesCambio.next(data);
          this.especialidadService.MensajeCambio.next("Se registró");
        });
      });
    }

    this.router.navigate(['especialidad']);

  }

}
