import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Especialidad } from 'src/app/_model/especialidad';
import { EspecialidadService } from 'src/app/_service/especialidad.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {

  displayedColumns = ['id', 'nombre', 'acciones'];
  dataSource: MatTableDataSource<Especialidad>;
  @ViewChild(MatPaginator, {static: true}) paginator : MatPaginator;
  @ViewChild(MatSort, {static: true}) sort : MatSort;

  constructor(private especialidadService: EspecialidadService, public route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.especialidadService.EspecialidadesCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.especialidadService.MensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'Aviso', {duration: 2000});
    });

    this.especialidadService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminar(idEspecialidad: number){
    this.especialidadService.eliminar(idEspecialidad).subscribe( () => {
      this.especialidadService.listar().subscribe( data => {
        this.especialidadService.EspecialidadesCambio.next(data);
        this.especialidadService.MensajeCambio.next("Se eliminó");
      });
    });
  }

}
