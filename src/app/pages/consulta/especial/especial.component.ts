import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Paciente } from 'src/app/_model/paciente';
import { Medico } from 'src/app/_model/medico';
import { Especialidad } from 'src/app/_model/especialidad';
import { Examen } from 'src/app/_model/examen';
import { DetalleConsulta } from 'src/app/_model/detalleConsulta';
import { Observable } from 'rxjs';
import { PacienteService } from 'src/app/_service/paciente.service';
import { MedicoService } from 'src/app/_service/medico.service';
import { EspecialidadService } from 'src/app/_service/especialidad.service';
import { ExamenService } from 'src/app/_service/examen.service';
import { MatSnackBar } from '@angular/material';
import { ConsultaService } from 'src/app/_service/consulta.service';
import { map } from 'rxjs/operators';
import { Consulta } from 'src/app/_model/consulta';
import { ConsultaListaExamenDTO } from 'src/app/_model/consultaListaExamenDTO';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-especial',
  templateUrl: './especial.component.html',
  styleUrls: ['./especial.component.css']
})
export class EspecialComponent implements OnInit {

  form: FormGroup;

  pacientes: Paciente[] = [];
  medicos: Medico[] = [];
  especialidades: Especialidad[] = [];
  examenes: Examen[] = [];

  detalleConsulta: DetalleConsulta[] = [];
  examenesSeleccionados: Examen[] = [];

  diagnostico: string;
  tratamiento: string;
  mensaje: string;

  fechaSeleccionado: Date = new Date();
  maxFecha: Date = new Date();

  consulta: Consulta;
  pacienteSeleccionado: Paciente;
  medicoSeleccionado: Medico;
  especialidadSeleccionada: Especialidad;
  examenSeleccionado: Examen;

  myControlPaciente: FormControl = new FormControl();
  myControlMedico: FormControl = new FormControl();

  filteredOptionsPaciente: Observable<any[]>;
  filteredOptionsMedico: Observable<any[]>;

  constructor(private builder: FormBuilder, private consultaService: ConsultaService, private pacienteService: PacienteService, private medicoService: MedicoService, private especialidadService: EspecialidadService, private examenService: ExamenService, public snackbar: MatSnackBar) { }

  ngOnInit() {

    this.form = this.builder.group({
      'paciente': this.myControlPaciente,
      'medico': this.myControlMedico,
      'especialidad': new FormControl(),
      'fecha': new FormControl(new Date()),
      'diagnostico': new FormControl(''),
      'tratamiento': new FormControl('')
    });

    this.listarPacientes();
    this.listarMedicos();
    this.listarEspecialidades();
    this.listarExamenes();

    this.filteredOptionsPaciente = this.myControlPaciente.valueChanges.pipe(map(val => this.filterPaciente(val)));
    this.filteredOptionsMedico = this.myControlMedico.valueChanges.pipe(map(val => this.filterMedico(val)));

  }

  filterPaciente(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option => 
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option => 
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  filterMedico(val: any) {
    if (val != null && val.idMedico > 0) {
      return this.medicos.filter(option => 
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.cmp.includes(val.cmp));
    } else {
      return this.medicos.filter(option => 
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.cmp.includes(val));
    }
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  listarMedicos() {
    this.medicoService.listar().subscribe(data => {
      this.medicos = data;
    });
  }

  listarEspecialidades() {
    this.especialidadService.listar().subscribe(data => {
      this.especialidades = data;
    });
  }

  listarExamenes() {
    this.examenService.listar().subscribe(data => {
      this.examenes = data;
    });
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  seleccionarMedico(e: any) {
    this.medicoSeleccionado = e.option.value;
  }

  displayPacienteFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  displayMedicoFn(val: Medico) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  agregarDetalle() {
    if (this.diagnostico != null && this.tratamiento != null) {
      let detalle = new DetalleConsulta();
      detalle.diagnostico = this.diagnostico;
      detalle.tratamiento = this.tratamiento;
      this.detalleConsulta.push(detalle);
    } else {
      this.mensaje = "Debe agregar un diagnóstico y un tratamiento";
      this.snackbar.open(this.mensaje, 'Aviso', { duration: 2000 });
    }
  }

  agregarExamen() {
    if (this.examenSeleccionado) {

      let cont = 0;
      for (let i = 0; i < this.examenesSeleccionados.length; i++) {
        let examen = this.examenesSeleccionados[i];
        if (examen.idExamen === this.examenSeleccionado.idExamen) {
          cont++;
          break;
        }
      }

      if (cont > 0) {
        this.mensaje = "El examen se encuentra en la lista";
        this.snackbar.open(this.mensaje, 'Aviso', { duration: 2000 });
      } else {
        this.examenesSeleccionados.push(this.examenSeleccionado);
      }

    } else {
      this.mensaje = "Debe agregar un examen";
      this.snackbar.open(this.mensaje, 'Aviso', { duration: 2000 });
    }
  }

  removerDetalle(index: number) {
    this.detalleConsulta.splice(index, 1);
  }

  removerExamen(index: number) {
    this.examenesSeleccionados.splice(index, 1);
  }

  estadoBotonRegistrar() {
    return (this.detalleConsulta.length === 0 || this.pacienteSeleccionado === null || this.medicoSeleccionado === null || this.especialidadSeleccionada === null);
  }

  aceptar() {
    this.consulta = new Consulta();
    this.consulta.paciente = this.form.value['paciente']; //this.pacienteSeleccionado
    this.consulta.medico = this.form.value['medico']; //this.medicoSeleccionado
    this.consulta.especialidad = this.form.value['especialidad']; //this.especialidadSeleccionada

    var tzoffset = (this.form.value['fecha']).getTimezoneOffset() * 60000;
    var localISOTime = (new Date(Date.now() - tzoffset)).toISOString();

    this.consulta.fecha = localISOTime;

    this.consulta.detalleConsulta = this.detalleConsulta;

    let consultaListaExamenDTO = new ConsultaListaExamenDTO();
    consultaListaExamenDTO.consulta = this.consulta;
    consultaListaExamenDTO.listaExamen = this.examenesSeleccionados;

    this.consultaService.registrar(consultaListaExamenDTO).subscribe(() => {
      this.snackbar.open("Se registró", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.limpiarControles();
      }, 2000);
    });

  }

  limpiarControles() {
    this.detalleConsulta = [];
    this.examenesSeleccionados = [];
    this.diagnostico = '';
    this.tratamiento = '';
    this.pacienteSeleccionado = null;
    this.especialidadSeleccionada = null;
    this.medicoSeleccionado = null;
    this.examenSeleccionado = null;
    this.fechaSeleccionado = new Date();
    this.fechaSeleccionado.setHours(0);
    this.fechaSeleccionado.setMinutes(0);
    this.fechaSeleccionado.setSeconds(0);
    this.fechaSeleccionado.setMilliseconds(0);
    this.mensaje = '';
    this.consulta = new Consulta();
    this.form.get('paciente').setValue('');
    this.form.get('medico').setValue('');
  }

}
