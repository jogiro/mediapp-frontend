import { Component, OnInit } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { Medico } from 'src/app/_model/medico';
import { Especialidad } from 'src/app/_model/especialidad';
import { Examen } from 'src/app/_model/examen';
import { ConsultaService } from 'src/app/_service/consulta.service';
import { PacienteService } from 'src/app/_service/paciente.service';
import { MedicoService } from 'src/app/_service/medico.service';
import { EspecialidadService } from 'src/app/_service/especialidad.service';
import { ExamenService } from 'src/app/_service/examen.service';
import { MatSnackBar } from '@angular/material';
import { DetalleConsulta } from 'src/app/_model/detalleConsulta';
import { Consulta } from 'src/app/_model/consulta';
import { ConsultaListaExamenDTO } from 'src/app/_model/consultaListaExamenDTO';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  pacientes: Paciente[] = [];
  medicos: Medico[] = [];
  especialidades: Especialidad[];
  examenes: Examen[];

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  detalleConsulta: DetalleConsulta[] = [];
  examenesSeleccionados: Examen[] = [];

  diagnostico: string;
  tratamiento: string;
  mensaje: string;

  idPacienteSeleccionado: number;
  idMedicoSeleccionado: number;
  idEspecialidadSeleccionada: number;
  idExamenSeleccionado: number;

  constructor(private consultaService: ConsultaService, private pacienteService: PacienteService, private medicoService: MedicoService, private especialidadService: EspecialidadService, private examenService: ExamenService, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.listarPacientes();
    this.listarMedicos();
    this.listarEspecialidades();
    this.listarExamenes();
  }

  listarPacientes(){
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  listarMedicos(){
    this.medicoService.listar().subscribe(data => {
      this.medicos = data;
    });
  }

  listarEspecialidades(){
    this.especialidadService.listar().subscribe(data => {
      this.especialidades = data;
    });
  }

  listarExamenes(){
    this.examenService.listar().subscribe(data => {
      this.examenes = data;
    });
  }

  agregarDetalle(){
   if(this.diagnostico != null && this.tratamiento != null){
     let detalle = new DetalleConsulta();
     detalle.diagnostico = this.diagnostico;
     detalle.tratamiento = this.tratamiento;

     this.detalleConsulta.push(detalle);

     this.diagnostico = null;
     this.tratamiento = null;
   }else{
    this.mensaje = "Debe agregar un diagnóstico y un tratamiento";
    this.snackbar.open(this.mensaje, 'Aviso', {duration: 2000});
   }
  }

  removerDetalle(index: number){
    this.detalleConsulta.splice(index);
  }

  agregarExamen(){
    if(this.idExamenSeleccionado > 0){

      let cont = 0;
      for(let i = 0; i < this.examenesSeleccionados.length; i++){
        let examen = this.examenesSeleccionados[i];
        if(examen.idExamen === this.idExamenSeleccionado){
          cont++;
          break;
        }
      }

      if(cont > 0){
        this.mensaje = "El examen se encuentra en la lista";
        this.snackbar.open(this.mensaje, 'Aviso', {duration: 2000});
      }else{
        let examen = new Examen();
        examen.idExamen = this.idExamenSeleccionado;
        this.examenService.listarId(this.idExamenSeleccionado).subscribe(data => {
          examen.nombre = data.nombre;
          examen.descripcion = data.descripcion;
          this.examenesSeleccionados.push(examen);
        });
      }
    }else{
      this.mensaje = "Debe agregar un examen";
      this.snackbar.open(this.mensaje, 'Aviso', {duration: 2000});
    }
  }

  removerExamen(index: number){
    this.examenesSeleccionados.splice(index);
  }

  estadoBotonRegistrar(){
    return (this.detalleConsulta.length === 0 || this.idPacienteSeleccionado === 0 || this.idMedicoSeleccionado === 0 || this.idEspecialidadSeleccionada === 0);
  }

  aceptar(){
    let paciente = new Paciente();
    paciente.idPaciente = this.idPacienteSeleccionado;

    let medico = new Medico();
    medico.idMedico = this.idMedicoSeleccionado;

    let especialidad = new Especialidad();
    especialidad.idEspecialidad = this.idEspecialidadSeleccionada;

    let consulta = new Consulta();
    consulta.paciente = paciente;
    consulta.medico = medico;
    consulta.especialidad = especialidad;

    //ISODATE
    let tzoffset = (this.fechaSeleccionada).getTimezoneOffset() * 60000;
    let localISOTime = (new Date(Date.now() - tzoffset)).toISOString();

    consulta.fecha = localISOTime;

    consulta.detalleConsulta = this.detalleConsulta;

    let consultaListaExamenDTO = new ConsultaListaExamenDTO();
    consultaListaExamenDTO.consulta = consulta;
    consultaListaExamenDTO.listaExamen = this.examenesSeleccionados;

    this.consultaService.registrar(consultaListaExamenDTO).subscribe(() => {
      this.snackbar.open("Se registró", "Aviso", {duration: 2000});

      setTimeout(() => {
        this.limpiarControles();
      }, 2000);
    });
  }

  limpiarControles(){
    this.detalleConsulta = [];
    this.examenesSeleccionados = [];
    this.diagnostico = '';
    this.tratamiento = '';
    this.idPacienteSeleccionado = 0;
    this.idMedicoSeleccionado = 0;
    this.idEspecialidadSeleccionada = 0;
    this.idExamenSeleccionado = 0;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.mensaje = '';
  }
}
