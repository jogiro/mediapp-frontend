import { Component, OnInit } from '@angular/core';
import { Examen } from 'src/app/_model/examen';
import { FormGroup, FormControl } from '@angular/forms';
import { ExamenService } from 'src/app/_service/examen.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-examen-edicion',
  templateUrl: './examen-edicion.component.html',
  styleUrls: ['./examen-edicion.component.css']
})
export class ExamenEdicionComponent implements OnInit {

  id: number;
  examen: Examen;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private examenService: ExamenService, private router: Router, private route: ActivatedRoute) {
    this.examen = new Examen();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl(''),
      'descripcion': new FormControl('')
    });

   }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  initForm(){
    if(this.edicion){
      this.examenService.listarId(this.id).subscribe(data => {
        let id = data.idExamen;
        let nombre = data.nombre;
        let descripcion = data.descripcion;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre),
          'descripcion': new FormControl(descripcion)
        });
      });
    }
  }


  operar(){
    this.examen.idExamen = this.form.value['id'];
    this.examen.nombre = this.form.value['nombre'];
    this.examen.descripcion = this.form.value['descripcion'];

    if(this.examen != null && this.examen.idExamen > 0){
      this.examenService.modificar(this.examen).subscribe( () => {
        this.examenService.listar().subscribe(data => {
          this.examenService.examenesCambio.next(data);
          this.examenService.mensajeCambio.next("Se modificó");
        });
      });
    }else{
      this.examenService.registrar(this.examen).subscribe( () => {
        this.examenService.listar().subscribe(data => {
          this.examenService.examenesCambio.next(data);
          this.examenService.mensajeCambio.next("Se registró");
        });
      });
    }

    this.router.navigate(['examen']);

  }

}
